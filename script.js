"use strict";
let player0_Score = 0;
let player1_Score = 0;
let activePlayerMode = true;
let player0_CurrentScore = 0;
let player1_CurrentScore = 0;

const changeActPlrLab = function () {
  if (activePlayerMode) {
    document.querySelector(".Pl1Sec").classList.add("player-Active");
    if (document.querySelector(".Pl2Sec").classList.contains("player-Active")) {
      document.querySelector(".Pl2Sec").classList.remove("player-Active");
    }
  } else {
    document.querySelector(".Pl2Sec").classList.add("player-Active");
    if (document.querySelector(".Pl1Sec").classList.contains("player-Active")) {
      document.querySelector(".Pl1Sec").classList.remove("player-Active");
    }
  }
};

const hideDice = function () {
  document.querySelector(".dice").classList.add("hidden");
};
const showDice = function () {
  document.querySelector(".dice").classList.remove("hidden");
};

changeActPlrLab();

document.querySelector(".btn--roll").addEventListener("click", function () {
  let randNum = Math.trunc(Math.random() * 5) + 1;

  showDice();
  document.querySelector(".dice").src = `images/dice-${String(randNum)}.png`;

  if (activePlayerMode) {
    if (randNum == 1) {
      player0_CurrentScore = 0;
      document.querySelector(".CrSc0").textContent =
        String(player0_CurrentScore);
      activePlayerMode = !activePlayerMode;
      hideDice();
    } else {
      player0_CurrentScore += randNum;
      document.querySelector(".CrSc0").textContent =
        String(player0_CurrentScore);
    }
  } else {
    if (randNum == 1) {
      player1_CurrentScore = 0;
      document.querySelector(".CrSc1").textContent =
        String(player1_CurrentScore);
      activePlayerMode = !activePlayerMode;
      hideDice();
    } else {
      player1_CurrentScore += randNum;
      document.querySelector(".CrSc1").textContent =
        String(player1_CurrentScore);
    }
  }
  changeActPlrLab();
});

document.querySelector(".btn--hold").addEventListener("click", function () {
  if (activePlayerMode) {
    player0_Score += player0_CurrentScore;
    player0_CurrentScore = 0;
    document.querySelector(".CrSc0").textContent = String(player0_CurrentScore);
    document.querySelector(".Sc0").textContent = String(player0_Score);
    activePlayerMode = !activePlayerMode;
    hideDice();
  } else {
    player1_Score += player1_CurrentScore;
    player1_CurrentScore = 0;
    document.querySelector(".CrSc1").textContent = String(player1_CurrentScore);
    document.querySelector(".Sc1").textContent = String(player1_Score);
    activePlayerMode = !activePlayerMode;
    hideDice();
  }
  changeActPlrLab();
  /////////winner section
  if (!activePlayerMode) {
    if (player0_Score >= 100) {
      document.querySelector(".Pl1Sec").classList.add("player-Winner");
      document.querySelector(".F1").textContent = "Player One Won 🎊";
    }
  } else {
    if (player1_Score >= 100) {
      document.querySelector(".Pl2Sec").classList.add("player-Winner");
      document.querySelector(".F2").textContent = "Player Two Won 🎊";
    }
  }
  ///////////////
});

//////Play again
document.querySelector(".playAgain").addEventListener("click", function () {
  player0_Score = 0;
  player1_Score = 0;
  activePlayerMode = true;
  player0_CurrentScore = 0;
  player1_CurrentScore = 0;
  changeActPlrLab();
  document.querySelector(".CrSc0").textContent = String(player0_CurrentScore);
  document.querySelector(".CrSc1").textContent = String(player1_CurrentScore);
  document.querySelector(".Sc0").textContent = String(player0_Score);
  document.querySelector(".Sc1").textContent = String(player1_Score);

  document.querySelector(".Pl1Sec").classList.remove("player-Winner");
  document.querySelector(".Pl2Sec").classList.remove("player-Winner");
  document.querySelector(".F1").textContent = "Player One";
  document.querySelector(".F2").textContent = "Player Two";
});
